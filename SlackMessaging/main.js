import {
  photonConnectUser,
  photonListenToMessage,
  photonSendMessage,
} from './PhotonChat.js';
import {
  slackFetchConversationHistory,
  slackFindConversation,
  slackListenToMessage,
  slackPublishMessage,
} from './utils.js';

export const slackUserIDs = ['U06FVR773LK', 'U06FXN5EX5Y', 'WHATEVER'];
const virtualSlackUser = [''];

export async function main() {
  // Find conversation with a specified channel `name`
  const conversationId = await slackFindConversation('test');
  // publishMessage(conversationId, 'Hello World!');
  photonConnectUser(slackUserIDs[0]);
  // slackFetchConversationHistory();
  slackListenToMessage((event) => {
    // console.log(event);
    if (event.subtype === undefined) {
      console.log(event);
      photonSendMessage(`User ${event.user} says ${event.text}`);
    }
    // photonConnectUser(slackUserIDs[1]);
  });
  photonListenToMessage((content, sender) => {
    slackPublishMessage(conversationId, content);
  });
  // photonSendMessage('Hello World!');
}
