import { app } from './index.js';

// Find conversation ID using the conversations.list method
export async function slackFindConversation(name) {
  try {
    // Call the conversations.list method using the built-in WebClient
    const result = await app.client.conversations.list({
      // The token you used to initialize your app
      token: process.env.SLACK_BOT_TOKEN,
    });

    for (const channel of result.channels) {
      if (channel.name === name) {
        const conversationId = channel.id;

        return conversationId;
      }
    }
  } catch (error) {
    console.error(error);
  }
}

// Post a message to a channel your app is in using ID and message text
export async function slackPublishMessage(id, text) {
  try {
    // Call the chat.postMessage method using the built-in WebClient
    const result = await app.client.chat.postMessage({
      // The token you used to initialize your app
      token: process.env.SLACK_BOT_TOKEN,
      channel: id,
      text: text,
      // You could also use a blocks[] array to send richer content
    });
  } catch (error) {
    console.error(error);
  }
}

export function slackListenToMessage(callback) {
  app.event('message', async ({ event, client }) => {
    try {
      // Call the chat.postMessage method using the built-in WebClient
      // console.log(event);
      // slackPublishMessage(event.channel, 'Listened');
      callback(event);
    } catch (error) {
      console.error(error);
    }
  });
}

export async function slackFetchConversationHistory() {
  const conversationId = 'C06GADG7RUH';
  try {
    // Call the conversations.history method using WebClient
    const result = await app.client.conversations.history({
      channel: conversationId,
    });

    console.log(result.messages);
  } catch (error) {
    console.error(error);
  }
}
