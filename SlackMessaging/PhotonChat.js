global.window = {};
global.Photon = {};
global.Photon.Chat = {};
global.Photon.Chat.ChatClient = {};

import dotenv from 'dotenv';
import fs from 'fs';
import { slackUserIDs } from './main.js';
import wsClient from 'ws';
global.wsClient = wsClient;
dotenv.config();

eval(fs.readFileSync('Photon/Photon-Javascript_SDK.js') + '');

var __extends =
  (this && this.__extends) ||
  (function () {
    var extendStatics = function (d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function (d, b) {
            d.__proto__ = b;
          }) ||
        function (d, b) {
          for (var p in b)
            if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function (d, b) {
      if (typeof b !== 'function' && b !== null)
        throw new TypeError(
          'Class extends value ' + String(b) + ' is not a constructor or null'
        );
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();

const PHOTON_CLIENT_APP_ID = process.env.PHOTON_CLIENT_APP_ID;
// const PHOTON_CLIENT_APP_ID = 'af55f49a-b556-4345-8031-7efca45ff50b';

const CHAT_CLIENT_CHANNEL_NAME = 'slack-messaging';

var SlackChatClient = /** @class */ (function (_super) {
  __extends(SlackChatClient, _super);
  function SlackChatClient() {
    var _this =
      _super.call(
        this,
        Photon.ConnectionProtocol.Wss,
        PHOTON_CLIENT_APP_ID,
        '1.0'
      ) || this;
    _this.callbacks = [];
    _this.chatListener = {};
    return _this;
  }
  SlackChatClient.prototype.addCallback = function (callback) {
    this.callbacks.push(callback);
  };
  SlackChatClient.prototype.onChatMessages = function (channelName, messages) {
    for (const message of messages) {
      const sender = message.getSender();
      const content = message.getContent();
      console.log('Message received: ', content, ' sent by: ', sender);
      if (this.chatListener[sender]) {
        for (const listener of this.chatListener[sender]) {
          listener(content, sender);
        }
      }
    }
  };
  SlackChatClient.prototype.onStateChange = function (state) {
    var ChatClientState = Photon.Chat.ChatClient.ChatState;
    if (state == ChatClientState.ConnectedToFrontEnd) {
      console.log(
        '[i]',
        '---- connected to Front End\n',
        "[Subscribe] for public channels or type in 'userid@message' and press 'Send' for private"
      );
      for (const func of this.callbacks) {
        func();
      }
      this.callbacks = [];
    }
    var disconnected =
      state == ChatClientState.Uninitialized ||
      state == ChatClientState.Disconnected;
    if (disconnected) {
      console.log('disconnected');
    }
  };
  SlackChatClient.prototype.addChatMessageListener = function (
    userId,
    listener
  ) {
    if (this.chatListener[userId]) {
      this.chatListener[userId].push(listener);
    } else {
      this.chatListener[userId] = [listener];
    }
  };
  SlackChatClient.prototype.clearChatMessageListener = function (userId) {
    if (userId === undefined) {
      this.chatListener = {};
    } else {
      this.chatListener[userId] = [];
    }
  };
  return SlackChatClient;
})(Photon.Chat.ChatClient);

const chatClient = new SlackChatClient();

export function photonConnectUser(userid, callback) {
  chatClient.disconnect();
  chatClient.setUserId(userid);
  chatClient.connectToRegionFrontEnd('US');
  chatClient.addCallback(() => {
    chatClient.subscribe([CHAT_CLIENT_CHANNEL_NAME]);
    chatClient.addFriends(slackUserIDs);
    console.log('[i] User connected --- ', userid);
    callback && callback();
  });
}

export function photonSendMessage(message) {
  chatClient.publishMessage(CHAT_CLIENT_CHANNEL_NAME, message);
  console.log('Message sent:', message);
}

export function photonListenToMessage(callback) {
  chatClient.addChatMessageListener(slackUserIDs[1], callback);
}
