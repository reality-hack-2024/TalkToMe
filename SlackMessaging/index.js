import bolt from '@slack/bolt';
import dotenv from 'dotenv';
import { main } from './main.js';
dotenv.config();

const { App } = bolt;

export const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  socketMode: true,
  appToken: process.env.SLACK_WEB_SOCKET_TOKEN,
});

(async () => {
  // Start your app
  await app.start();
  await main();
})();
