using System;
using System.Collections.Generic;
using TMPro;

using UnityEngine;
using UnityEngine.UI;

using Photon.Chat;
using Photon.Realtime;
using AuthenticationValues = Photon.Chat.AuthenticationValues;
#if PHOTON_UNITY_NETWORKING
using Photon.Pun;
#endif


namespace Photon.Chat.Demo
{
    public class PhotonChat : MonoBehaviour, IChatClientListener
    {

        private string[] ChannelsToJoinOnConnect; // set in inspector. Demo channels to join automatically.

        //public string[] FriendsList;

        private int HistoryLengthToFetch; // set in inspector. Up to a certain degree, previously sent messages can be fetched for context

        private string UserName { get; set; }

        private string selectedChannelName; // mainly used for GUI/input

        public ChatClient chatClient;
        public TMP_Text readText;
        public GameObject avatar;
        private animator_movement avatarController;

        #if !PHOTON_UNITY_NETWORKING
        public ChatAppSettings ChatAppSettings
        {
            get { return this.chatAppSettings; }
        }

        [SerializeField]
        #endif
        protected internal ChatAppSettings chatAppSettings;


        // public InputField InputFieldChat;   // set in inspector
        // public Text CurrentChannelText;     // set in inspector
        // public Toggle ChannelToggleToInstantiate; // set in inspector

        private readonly Dictionary<string, Toggle> channelToggles = new Dictionary<string, Toggle>();

        private bool ShowState = true;
        // public Text StateText; // set in inspector
        // public Text UserIdText; // set in inspector
        public void Start()
        {
            DontDestroyOnLoad(this.gameObject);

            this.UserName = "UNITY_USER"; //made-up username

            #if PHOTON_UNITY_NETWORKING
            this.chatAppSettings = PhotonNetwork.PhotonServerSettings.AppSettings.GetChatSettings();
            #endif

            bool appIdPresent = !string.IsNullOrEmpty(this.chatAppSettings.AppIdChat);

            if (!appIdPresent)
            {
                Debug.LogError("You need to set the chat app ID in the PhotonServerSettings file in order to continue.");
            }
            this.ChannelsToJoinOnConnect = new string[] {"slack-messaging" };  
            this.Connect();

            avatarController = avatar.GetComponent<animator_movement>();
        }

        public void Connect()
        {

            this.chatClient = new ChatClient(this);
            #if !UNITY_WEBGL
            this.chatClient.UseBackgroundWorkerForSending = true;
            #endif
            this.chatClient.AuthValues = new AuthenticationValues(this.UserName);
            this.chatClient.ConnectUsingSettings(this.chatAppSettings);

            //this.ChannelToggleToInstantiate.gameObject.SetActive(false);
            Debug.Log("Connecting as: " + this.UserName);

        }

        /// <summary>To avoid that the Editor becomes unresponsive, disconnect all Photon connections in OnDestroy.</summary>
        public void OnDestroy()
        {
            if (this.chatClient != null)
            {
                this.chatClient.Disconnect();
            }
        }

        /// <summary>To avoid that the Editor becomes unresponsive, disconnect all Photon connections in OnApplicationQuit.</summary>
        public void OnApplicationQuit()
        {
            if (this.chatClient != null)
            {
                this.chatClient.Disconnect();
            }
        }

        public void Update()
        {
            if (this.chatClient != null)
            {
                this.chatClient.Service(); // make sure to call this regularly! it limits effort internally, so calling often is ok!
            }

            // check if we are missing context, which means we got kicked out to get back to the Photon Demo hub.
            // if ( this.StateText == null)
            // {
            //     Destroy(this.gameObject);
            //     return;
            // }

            // this.StateText.gameObject.SetActive(this.ShowState); // this could be handled more elegantly, but for the demo it's ok.
        }


        // public void OnEnterSend()
        // {
        //     if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        //     {
        //         this.SendChatMessage(this.InputFieldChat.text);
        //         this.InputFieldChat.text = "";
        //     }
        // }

        // public void OnClickSend()
        // {
        //     if (this.InputFieldChat != null)
        //     {
        //         this.SendChatMessage(this.InputFieldChat.text);
        //         this.InputFieldChat.text = "";
        //     }
        // }


        public int TestLength = 2048;
        private byte[] testBytes = new byte[2048];

        public void SendChatMessage(string inputLine)
        {
            if (string.IsNullOrEmpty(inputLine))
            {
                return;
            }
            if ("test".Equals(inputLine))
            {
                if (this.TestLength != this.testBytes.Length)
                {
                    this.testBytes = new byte[this.TestLength];
                }

                this.chatClient.SendPrivateMessage(this.chatClient.AuthValues.UserId, this.testBytes, true);
            }


            bool doingPrivateChat = this.chatClient.PrivateChannels.ContainsKey(this.selectedChannelName);
            string privateChatTarget = string.Empty;
            if (doingPrivateChat)
            {
                // the channel name for a private conversation is (on the client!!) always composed of both user's IDs: "this:remote"
                // so the remote ID is simple to figure out

                string[] splitNames = this.selectedChannelName.Split(new char[] { ':' });
                privateChatTarget = splitNames[1];
            }


            if (inputLine[0].Equals('\\'))
            {
                string[] tokens = inputLine.Split(new char[] {' '}, 2);
                if (tokens[0].Equals("\\msg") && !string.IsNullOrEmpty(tokens[1]))
                {
                    string[] subtokens = tokens[1].Split(new char[] {' ', ','}, 2);
                    if (subtokens.Length < 2) return;

                    string targetUser = subtokens[0];
                    string message = subtokens[1];
                    this.chatClient.SendPrivateMessage(targetUser, message);
                }
                #if CHAT_EXTENDED
                else if ((tokens[0].Equals("\\nickname") || tokens[0].Equals("\\nick") ||tokens[0].Equals("\\n")) && !string.IsNullOrEmpty(tokens[1]))
                {
                    if (!doingPrivateChat)
                    {
                        this.chatClient.SetCustomUserProperties(this.selectedChannelName, this.chatClient.UserId, new Dictionary<string, object> {{"Nickname", tokens[1]}});
                    }

                }
                #endif
                else
                {
                    Debug.Log("The command '" + tokens[0] + "' is invalid.");
                }
            }
            else
            {
                if (doingPrivateChat)
                {
                    this.chatClient.SendPrivateMessage(privateChatTarget, inputLine);
                }
                else
                {
                    this.chatClient.PublishMessage(this.selectedChannelName, inputLine);
                }
            }
        }

        public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
        {
            if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
            {
                Debug.LogError(message);
            }
            else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
            {
                Debug.LogWarning(message);
            }
            else
            {
                Debug.Log(message);
            }
        }

        public void OnConnected()
        {
            if (this.ChannelsToJoinOnConnect != null && this.ChannelsToJoinOnConnect.Length > 0)
            {
                this.chatClient.Subscribe(this.ChannelsToJoinOnConnect, this.HistoryLengthToFetch);
            }

            this.chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).
        }

        public void OnDisconnected()
        {
            Debug.Log("OnDisconnected()");
        }

        public void OnChatStateChange(ChatState state)
        {
            // use OnConnected() and OnDisconnected()
            // this method might become more useful in the future, when more complex states are being used.

        }

        public void OnSubscribed(string[] channels, bool[] results)
        {

            Debug.Log("OnSubscribed: " + string.Join(", ", channels));

            // Switch to the first newly created channel
            this.ShowChannel(channels[0]);
        }

        /// <inheritdoc />
        public void OnSubscribed(string channel, string[] users, Dictionary<object, object> properties)
        {
            Debug.LogFormat("OnSubscribed: {0}, users.Count: {1} Channel-props: {2}.", channel, users.Length, properties.ToStringFull());
        }

        // private void InstantiateChannelButton(string channelName)
        // {
        //     if (this.channelToggles.ContainsKey(channelName))
        //     {
        //         Debug.Log("Skipping creation for an existing channel toggle.");
        //         return;
        //     }

        //     Toggle cbtn = (Toggle)Instantiate(this.ChannelToggleToInstantiate);
        //     cbtn.gameObject.SetActive(true);
        //     cbtn.GetComponentInChildren<ChannelSelector>().SetChannel(channelName);
        //     cbtn.transform.SetParent(this.ChannelToggleToInstantiate.transform.parent, false);

        //     this.channelToggles.Add(channelName, cbtn);
        // }


        public void OnUnsubscribed(string[] channels)
        {
            foreach (string channelName in channels)
            {
                if (this.channelToggles.ContainsKey(channelName))
                {
                    Toggle t = this.channelToggles[channelName];
                    Destroy(t.gameObject);

                    this.channelToggles.Remove(channelName);

                    Debug.Log("Unsubscribed from channel '" + channelName + "'.");

                    // Showing another channel if the active channel is the one we unsubscribed from before
                    if (channelName == this.selectedChannelName && this.channelToggles.Count > 0)
                    {
                        IEnumerator<KeyValuePair<string, Toggle>> firstEntry = this.channelToggles.GetEnumerator();
                        firstEntry.MoveNext();

                        this.ShowChannel(firstEntry.Current.Key);

                        firstEntry.Current.Value.isOn = true;
                    }
                }
                else
                {
                    Debug.Log("Can't unsubscribe from channel '" + channelName + "' because you are currently not subscribed to it.");
                }
            }
        }

        public void OnGetMessages(string channelName, string[] senders, object[] messages)
        {
            if (channelName.Equals(this.selectedChannelName))
            {
                // update text
                print("inside the function");
                this.ShowChannel(this.selectedChannelName);
            }
        }

        public void OnPrivateMessage(string sender, object message, string channelName)
        {
            // as the ChatClient is buffering the messages for you, this GUI doesn't need to do anything here
            // you also get messages that you sent yourself. in that case, the channelName is determinded by the target of your msg
            // this.InstantiateChannelButton(channelName);

            byte[] msgBytes = message as byte[];
            if (msgBytes != null)
            {
                Debug.Log("Message with byte[].Length: "+ msgBytes.Length);
            }
            if (this.selectedChannelName.Equals(channelName))
            {
                this.ShowChannel(channelName);
            }
        }
        public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
        {

            Debug.LogWarning("status: " + string.Format("{0} is {1}. Msg:{2}", user, status, message));

            // if (this.friendListItemLUT.ContainsKey(user))
            // {
            //     FriendItem _friendItem = this.friendListItemLUT[user];
            //     if ( _friendItem!=null) _friendItem.OnFriendStatusUpdate(status,gotMessage,message);
            // }
        }

        public void OnUserSubscribed(string channel, string user)
        {
            Debug.LogFormat("OnUserSubscribed: channel=\"{0}\" userId=\"{1}\"", channel, user);
        }

        public void OnUserUnsubscribed(string channel, string user)
        {
            Debug.LogFormat("OnUserUnsubscribed: channel=\"{0}\" userId=\"{1}\"", channel, user);
        }

        /// <inheritdoc />
        public void OnChannelPropertiesChanged(string channel, string userId, Dictionary<object, object> properties)
        {
            Debug.LogFormat("OnChannelPropertiesChanged: {0} by {1}. Props: {2}.", channel, userId, Extensions.ToStringFull(properties));
        }

        public void OnUserPropertiesChanged(string channel, string targetUserId, string senderUserId, Dictionary<object, object> properties)
        {
            Debug.LogFormat("OnUserPropertiesChanged: (channel:{0} user:{1}) by {2}. Props: {3}.", channel, targetUserId, senderUserId, Extensions.ToStringFull(properties));
        }

        /// <inheritdoc />
        public void OnErrorInfo(string channel, string error, object data)
        {
            Debug.LogFormat("OnErrorInfo for channel {0}. Error: {1} Data: {2}", channel, error, data);
        }

        public void AddMessageToSelectedChannel(string msg)
        {
            ChatChannel channel = null;
            bool found = this.chatClient.TryGetChannel(this.selectedChannelName, out channel);
            if (!found)
            {
                Debug.Log("AddMessageToSelectedChannel failed to find channel: " + this.selectedChannelName);
                return;
            }

            if (channel != null)
            {
                channel.Add("Bot", msg,0); //TODO: how to use msgID?
            }
        }

        public void ShowChannel(string channelName)
        {
            if (string.IsNullOrEmpty(channelName))
            {
                return;
            }

            ChatChannel channel = null;
            bool found = this.chatClient.TryGetChannel(channelName, out channel);
            if (!found)
            {
                Debug.Log("ShowChannel failed to find channel: " + channelName);
                return;
            }

            this.selectedChannelName = channelName;
            // this.CurrentChannelText.text = channel.ToStringMessages();
            if (channel.Messages.Count > 0)
            {
                for (int i = channel.Messages.Count - 1; i>=0; i--) {
                    if (channel.Senders[i] != "UNITY_USER") {
                        print(channel.Messages[0].ToString());
                        readText.text = channel.Messages[0].ToString();
                        avatarController.setInputTextStage();
                        break;
                    }
                }
                channel.ClearMessages();
            }
            Debug.Log("ShowChannel: " + this.selectedChannelName);

            foreach (KeyValuePair<string, Toggle> pair in this.channelToggles)
            {
                pair.Value.isOn = pair.Key == channelName ? true : false;
            }
        }

        public void OpenDashboard()
        {
            Application.OpenURL("https://dashboard.photonengine.com");
        }




    }
}