using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GorillaZilla
{
    /// <summary>
    ///     This component enables smoothly switching between OVRPassthrough Layers
    /// </summary>
    public class PassthroughLayerController : MonoBehaviour
    {
        public float layerTransitionSmoothing = 1f;
        public OVRPassthroughLayer defaultLayer;
        public OVRPassthroughLayer listeningLayer;
        private OVRPassthroughLayer activeLayer;
        private OVRPassthroughLayer[] passthroughLayers;
        private void Awake()
        {
            passthroughLayers = GameObject.FindObjectsByType<OVRPassthroughLayer>(FindObjectsSortMode.None);
        }
        public void SetActiveLayer(OVRPassthroughLayer layer)
        {
            activeLayer = layer;
        }
        // public void SetActiveLayer(string layerName)
        // {
        //     activeLayer = GetPassthroughLayer(layerName);
        // }
        public void SetDefaultLayer()
        {
            activeLayer = defaultLayer;
        }
        
        public void SetListeningLayer()
        {
            activeLayer = listeningLayer;
        }
        // private OVRPassthroughLayer GetPassthroughLayer(string layerName)
        // {
        //     OVRPassthroughLayer active = null;
        //     foreach (var layer in passthroughLayers)
        //     {
        //         if (layer.name.Contains(layerName))
        //         {
        //             active = layer;
        //         }
        //     }
        //     return active;
        // }

        //Fades out each layer to 0 opacity, and fades in active layer to full opacity
        // private void FadeIn()
        // {
        //     foreach (var layer in passthroughLayers)
        //     {
        //         if (layer == activeLayer)
        //         {
        //             layer.textureOpacity = Mathf.Clamp01(layer.textureOpacity + (Time.deltaTime * layerTransitionSmoothing));
        //         }
        //         else
        //         {
        //             layer.textureOpacity = Mathf.Clamp01(layer.textureOpacity - (Time.deltaTime * layerTransitionSmoothing));
        //         }
        //     }
        // }
        private void FadeInDefault()
        {
            foreach (var layer in passthroughLayers)
            {
                if (layer == activeLayer)
                {
                    layer.textureOpacity = Mathf.Clamp01(layer.textureOpacity + (Time.deltaTime * layerTransitionSmoothing));
                }
                else
                {
                    layer.textureOpacity = Mathf.Clamp01(layer.textureOpacity - (Time.deltaTime * layerTransitionSmoothing));
                }
            }
        }
        private void FadeInListening()
        {
            foreach (var layer in passthroughLayers)
            {
                if (layer == activeLayer)
                {
                    layer.textureOpacity = Mathf.Clamp(layer.textureOpacity + (Time.deltaTime * layerTransitionSmoothing), 0f, 0.1f);
                }
                else
                {
                    layer.textureOpacity = Mathf.Clamp(layer.textureOpacity - (Time.deltaTime * layerTransitionSmoothing), 0.9f, 1.0f);
                }
            }
        }
        private void Update()
        {
            if (activeLayer == defaultLayer && activeLayer.textureOpacity < 1)
            {
                FadeInDefault();
            }
            if (activeLayer == listeningLayer && listeningLayer.textureOpacity < 0.1)
            {
                FadeInListening();
            }
        }
    }
}

