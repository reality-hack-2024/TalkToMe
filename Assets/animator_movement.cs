using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta.WitAi.TTS.Utilities;
using TMPro;
using System;

public class animator_movement : MonoBehaviour
{
    public GameObject avatar; 
    Animator m_Animator;
    private Vector3 startWalkingPosition;
    private Vector3 endWalkingPosition;
    private Vector3 disappearPosition;
    private bool isWalking;
    private bool isReturning;
    private bool setChangeAngleReturn;
    private bool walk2Idle;
    private bool walk2Talk;
    private float keyDelay = 1f;  // 1 second
    private float timePassed = 0f;
    public GameObject textMessageIn;
    private bool speakerEnds;
    public bool inputTextStage;
    public bool outputSpeakStageStart;
    public bool outputSpeakStageEnd;
    public bool callAvatarToSpeakStageStart;
    public bool callAvatarToSpeakStageEnd;
    private int waitTimeSecond;
    private float secondsCount;
    private bool callTowardEnds;
    public GameObject avatarModel;
    private Material avatarMaterial;
    private Renderer renderer;
    private Color avatarMaterialColor;
    public GameObject transcriptorLabel;
    void Start()
    {
        //Get the Animator attached to the GameObject you are intending to animate.
        m_Animator = gameObject.GetComponent<Animator>();
        isWalking = false;
        isReturning = false;
        disappearPosition = avatar.transform.position;
        endWalkingPosition = new Vector3(1, disappearPosition.y, disappearPosition.z);
        startWalkingPosition = new Vector3(5, disappearPosition.y, disappearPosition.z);
        setChangeAngleReturn = false;
        walk2Idle = false;
        walk2Talk = false;
        speakerEnds = false; 
        inputTextStage = false;
        outputSpeakStageStart = false;
        outputSpeakStageEnd = false;
        callAvatarToSpeakStageStart = false;
        callAvatarToSpeakStageEnd = false;
        waitTimeSecond = 30;
        secondsCount = 0f;
        callTowardEnds = false;
        // wait 30 seconds, see if there's any trigger from the controller
        transcriptorLabel.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {   
        if (inputTextStage)
        {
            // idle --> walking to center --> talking
            m_Animator.SetBool("Idle", false);
            m_Animator.SetBool("Walking", true);
            avatar.transform.position = startWalkingPosition;
            isWalking = true;
            walk2Talk = true;
            m_Animator.SetBool("talk2idle", false);
            m_Animator.SetBool("idle2talk", false);
            inputTextStage = false;
            // // transparent --> opaque
            // avatarMaterialColor.a = 1f;
            // renderer.material.color = avatarMaterialColor;
        }
        if (callAvatarToSpeakStageStart)
        {
            // call avatar toward headset and talk to the avatar, 
            // event triggered by pressing the record speech button 
            // idle --> walking to center --> idle
            m_Animator.SetBool("Idle", false);
            m_Animator.SetBool("Walking", true);
            avatar.transform.position = startWalkingPosition;
            isWalking = true;
            walk2Idle = true;
            m_Animator.SetBool("talk2idle", false);
            m_Animator.SetBool("idle2talk", false);
            callAvatarToSpeakStageStart = false;
            callTowardEnds = true;
            // // transparent --> opaque
            // avatarMaterialColor.a = 1f;
            // renderer.material.color = avatarMaterialColor;
        }
        if (callTowardEnds)
        {
            // keep as idle
            secondsCount += Time.deltaTime;
            if (secondsCount >= waitTimeSecond && !outputSpeakStageStart)
            {
                // waited for 30 seconds and no response, just walk away
                // else talking --> walking to edge --> idle
                isReturning = true;
                m_Animator.SetBool("Idle", false);
                m_Animator.SetBool("Walking", true);
                setChangeAngleReturn = true;
                Vector3 rotationToAdd = new Vector3(0, -90, 0);
                avatar.transform.Rotate(rotationToAdd);
                setChangeAngleReturn = false;
                timePassed = 0f;
                m_Animator.SetBool("talk2idle", false);
                m_Animator.SetBool("idle2talk", false);
                speakerEnds = false;
                secondsCount = 0f;
            }
            else
            {
                if (outputSpeakStageStart)
                {
                    // has response within 30 seconds
                    if (outputSpeakStageEnd)
                    {
                        // speak ends and walk away
                        // else talking --> walking to edge --> idle
                        isReturning = true;
                        m_Animator.SetBool("Idle", false);
                        m_Animator.SetBool("Walking", true);
                        setChangeAngleReturn = true;
                        Vector3 rotationToAdd = new Vector3(0, -90, 0);
                        avatar.transform.Rotate(rotationToAdd);
                        setChangeAngleReturn = false;
                        timePassed = 0f;
                        m_Animator.SetBool("talk2idle", false);
                        m_Animator.SetBool("idle2talk", false);
                        speakerEnds = false;
                        outputSpeakStageStart = false;
                        outputSpeakStageEnd = false;
                        secondsCount = 0f;
                    }
                }
            }
        }

        if (isWalking)
        {
            print("DEBUG: in isWalking");
            if (avatar.transform.position.x >= endWalkingPosition.x)
            {
                print("DEBUG: in keep walking");
                // keep walking
                avatar.transform.position -= new Vector3(0.02f, 0, 0);
                avatar.transform.position = avatar.transform.position;
                print("after::" + avatar.transform.position);
                    //Press the up arrow button to reset the trigger and set another one
            }
            else 
            {
                print("DEBUG: not in keep walking");
                if (walk2Talk)
                {
                    print("DEBUG: walk 2 talk");
                    m_Animator.SetBool("Walking", false);
                    //Send the message to the Animator to activate the trigger parameter named "Jump"
                    m_Animator.SetBool("Talking", true);
                    Vector3 rotationToAdd = new Vector3(0, -90, 0);
                    avatar.transform.Rotate(rotationToAdd);
                    // tts.OnEnable();
                    textMessageIn.SetActive(true);
                    print("finished set active");
                    print("tex message hierchy" + textMessageIn.activeInHierarchy);
                    isWalking = false;
                    walk2Talk = false;
                    speakerEnds = true;
                }
                if (walk2Idle)
                {
                    print("DEBUG: walk 2 idle");
                    m_Animator.SetBool("Walking", false);
                    m_Animator.SetBool("Idle", true);
                    Vector3 rotationToAdd = new Vector3(0, -90, 0);
                    avatar.transform.Rotate(rotationToAdd);
                    isWalking = false;
                    walk2Idle = false;
                }
            }
        }
        if (speakerEnds && textMessageIn.activeInHierarchy == false)
        {
            
            // talking --> idle
            m_Animator.SetBool("Talking", false);
            m_Animator.SetBool("talk2idle", true);
            m_Animator.SetBool("idle2talk", false);
            secondsCount += Time.deltaTime;
            if (secondsCount >= waitTimeSecond && !outputSpeakStageStart)
            {
                // waited for 30 seconds and no response, just walk away
                // else talking --> walking to edge --> idle
                isReturning = true;
                m_Animator.SetBool("Idle", false);
                m_Animator.SetBool("Walking", true);
                setChangeAngleReturn = true;
                Vector3 rotationToAdd = new Vector3(0, -90, 0);
                avatar.transform.Rotate(rotationToAdd);
                setChangeAngleReturn = false;
                timePassed = 0f;
                m_Animator.SetBool("talk2idle", false);
                m_Animator.SetBool("idle2talk", false);
                speakerEnds = false;
                secondsCount = 0f;
            }
            else
            {
                if (outputSpeakStageStart)
                {
                    // has response within 30 seconds
                    if (outputSpeakStageEnd)
                    {
                        // speak ends and walk away
                        // else talking --> walking to edge --> idle
                        isReturning = true;
                        m_Animator.SetBool("Idle", false);
                        m_Animator.SetBool("Walking", true);
                        setChangeAngleReturn = true;
                        Vector3 rotationToAdd = new Vector3(0, -90, 0);
                        avatar.transform.Rotate(rotationToAdd);
                        setChangeAngleReturn = false;
                        timePassed = 0f;
                        m_Animator.SetBool("talk2idle", false);
                        m_Animator.SetBool("idle2talk", false);
                        speakerEnds = false;
                        outputSpeakStageStart = false;
                        outputSpeakStageEnd = false;
                        secondsCount = 0f;
                    }
                }
            }
        }

        if (callAvatarToSpeakStageEnd) 
        {
            // finishes talking to the avatar, avatar leaves
            // idle --> walking to edge --> idle
            isReturning = true;
            m_Animator.SetBool("Idle", false);
            m_Animator.SetBool("Walking", true);
            setChangeAngleReturn = true;
            Vector3 rotationToAdd = new Vector3(0, -90, 0);
            avatar.transform.Rotate(rotationToAdd);
            setChangeAngleReturn = false;
            timePassed = 0f;
            m_Animator.SetBool("talk2idle", false);
            m_Animator.SetBool("idle2talk", false);
            callAvatarToSpeakStageEnd = false;
        }
        if (isReturning)
        {
            if (avatar.transform.position.x < startWalkingPosition.x)
            {
                avatar.transform.position += new Vector3(0.02f, 0, 0);
                avatar.transform.position = avatar.transform.position;
                print("after:: in returning" + avatar.transform.position);
                //Press the up arrow button to reset the trigger and set another one
            }
            else
            {
                m_Animator.SetBool("Walking", false);
                //Send the message to the Animator to activate the trigger parameter named "Jump"
                m_Animator.SetBool("Idle", true);
                avatar.transform.Rotate(0.0f, -180.0f, 0.0f, Space.Self);
                isReturning = false;
                avatar.transform.position = disappearPosition;
                // TODO: change the trasncriptor setting so it waits for 30 seconds before it 
                transcriptorLabel.SetActive(false);
                TMP_Text transcriptorText = transcriptorLabel.GetComponent<TMP_Text>();
                transcriptorText.text = "";
            }
        }
    }
    public void setInputTextStage() 
    {
        inputTextStage = true;
    }
    public void setoutputSpeakStageStart() 
    {
        outputSpeakStageStart = true;
        transcriptorLabel.SetActive(true);
    }
    public void setoutputSpeakStageEnd() 
    {
        outputSpeakStageEnd = true;
        print("DEBUG: activate outputSpeakStageEnd, should be true" + outputSpeakStageEnd);
    }
    public void setCallAvatarToSpeakStageStart() 
    {
        callAvatarToSpeakStageStart = true;
    }
    public void setCallAvatarToSpeakStageEnd() 
    {
        callAvatarToSpeakStageEnd = true;
        print("DEBUG: activate callAvatarToSpeakStageEnd, should be true" + callAvatarToSpeakStageEnd);
    }
}
