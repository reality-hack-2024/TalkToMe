# Talk To Me

**Members:** Ananth Nayak, Kathy Zhuang, Ray Sun, Yuanbo Chen, Yukun Song

## Setup

### Hardware Required

- git lfs
- Meta Quest 3
- Unity 2022.3.x

### Software Dependencies

- Division Game Engine version `2024.1.26`
- Michaelsoft Binbows `XD`

## Shout-Outs

- Meta Presence Platform
- All the brilliant mentors that helped us out!
